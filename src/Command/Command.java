package Command;
import Strategy.*;
public abstract class Command {
	Receiver receiver;
	public abstract void execute();
	public void setReceiver(Receiver receiver){
		this.receiver = receiver;
	}
}
