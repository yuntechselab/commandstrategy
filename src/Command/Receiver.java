package Command;

import Strategy.*;

public abstract class Receiver {
	Strategy strategy;

	public abstract void turnOff();

	public abstract void turnOn();

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	};

	

}
