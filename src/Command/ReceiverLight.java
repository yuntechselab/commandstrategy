package Command;

import Strategy.*;

public class ReceiverLight extends Receiver {

	@Override
	public void turnOff() {
		// TODO Auto-generated method stub
		System.out.println("ReceiverLight turn off");
		new LightOnStrategy().action();

	}

	@Override
	public void turnOn() {
		// TODO Auto-generated method stub
		System.out.println("ReceiverLight turn on");

	}

	

}
