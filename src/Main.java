import Command.Command;
import Command.Invoker_RemoteControl;
import Command.ReceiverLight;
import Command.Receiver;
import Command.ReceiverTv;
import Command.TurnOffCommand;
import Command.TurnOnCommand;

public class Main {
	public static void main(String[] args) {
		Invoker_RemoteControl ir = new Invoker_RemoteControl();
		Receiver tv = new ReceiverTv();
		Command turnOnCommand = new TurnOnCommand();
	
		turnOnCommand.setReceiver(tv);
		ir.setCommand(turnOnCommand);
		ir.pressButton();
	
	}
}
