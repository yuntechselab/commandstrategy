package Strategy;

public abstract class Strategy {
	public abstract void action();
}
